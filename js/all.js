Number.prototype.format = function (n, x, s, c) {
	//http://stackoverflow.com/users/1249581/vision
	'use strict';
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};
function declOfNum(titles) {
	//https://gist.github.com/retyui
	'use strict';
    var number,
		cases;
	number = Math.abs(number);
    cases = [2, 0, 1, 1, 1, 2];
    return function (number) {
        return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
    };
}
var count_rubl = declOfNum(['рубль', 'рубля', 'рублей']),
	count_mounth = declOfNum(['месяц', 'месяца', 'месяцев']);
$(function () {
	'use strict';
	var $doc_ob = $(document);
	
	function formula(S, N, P) {
		var value;
		value = parseInt((S * (P + (P / (Math.pow(1 + P, N) - 1)))), 10);
		return value;
	}
	
	function drag_line($el) {
		//подсветка перед указателем
		var line_width = parseInt($el.css('left'), 10) + ($el.outerWidth(true) / 2);
		$el.siblings('.drag_line').css('width', line_width);
	}
	
	function move_pointer() {
		//двигаем указатели
		var max,
			delta,
			$el,
			value,
			max_value,
			css_value;
		$('.column').each(function () {
			$el = $(this).find('.drag');
			delta = parseInt($el.attr('min'), 10);
			max = parseInt($el.attr('max'), 10) - delta;
			value = parseInt($el.siblings('.fl').find('input').val(), 10) - delta;
			max_value = $el.outerWidth(true) - $el.children('.drag_pointer').outerWidth(true);
			css_value = (max_value * value) / max;
			$el.children('.drag_pointer').css('left', css_value);
			drag_line($el.children('.drag_pointer'));
		});
	}
	
	function formula_set() {
		//получаем минимальный, максимальный и текущий месячный платеж
		var min,
			max,
			current,
			S = parseInt($('.column').eq(0).find('input').val(), 10),
			N = parseInt($('.column').eq(1).find('input').val(), 10),
			$target = $('.column').eq(2),
			work_var;
		min = formula(S, parseInt($('.column').eq(1).find('.drag').attr('max'), 10), 0.18);
		max = formula(S, parseInt($('.column').eq(1).find('.drag').attr('min'), 10), 0.18);
		current = formula(S, N, 0.18);
		$target.find('input').val(current);
		$target.find('.drag').attr('min', min);
		$target.find('.drag').attr('max', max);
		move_pointer();
		work_var = current;
		$('#mount_result span').eq(0).text(work_var.format(0, 3, ' '));
		$('#mount_result span').eq(1).text(count_rubl(current));
		$('.count_mount').text(count_mounth(N));
		$('#mount_count span').eq(0).text(N);
		$('#mount_count span').eq(1).text(count_mounth(N));
		$('#money_get span').eq(0).text(S.format(0, 3, ' '));
		$('#money_get span').eq(1).text(count_rubl(S));
	}
	
	
	function getChar(event) {
		//проверяем нажатую кнопку
		if (event.which === null) {
			if (event.keyCode < 32) {
				return null;
			}
			return String.fromCharCode(event.keyCode); // IE
		}
		if (event.which !== 0 && event.charCode !== 0) {
			if (event.which < 32) {
				return null;
			}
			return String.fromCharCode(event.which); // остальные
		}
		return null; // специальная клавиша
	}
	$doc_ob.on('keypress', 'input', function (e) {
		//блокируем ввод символов в input, разрешенно только цифры
		e = e || event;
		if (e.ctrlKey || e.altKey || e.metaKey) {
			return;
		}
		var chr = getChar(e);
		if (e.keyCode === 13) {
			$(this).blur();
		}
		if (chr === null) {
			return;
		}
		if (chr < '0' || chr > '9') {
			return false;
		}
	});
	
	$doc_ob.on('change', 'input', function (e) {
		//проверяем правильность введеных данных
		var $el = $(this),
			$parent = $el.parents('.column');

		if (parseInt($el.val(), 10) < $parent.find('.drag').attr('min')) {
			$el.val($parent.find('.drag').attr('min'));
		}
		if (parseInt($el.val(), 10) > $parent.find('.drag').attr('max')) {
			$el.val($parent.find('.drag').attr('max'));
		}
		formula_set();
	});
	
	
	
	
	function move_value($el, mouse) {
		//проверяем движение указателя в заданных пределах
		var page_offset = $el.parents('.drag').offset().left,
			value = mouse - page_offset,
			max = $el.parents('.drag').outerWidth(true) - $el.outerWidth(true);
		if (value <= 0) {
			value = 0;
		}
		if (value >= max) {
			value = max;
		}
		return value;
	}
	
	$('.drag_pointer').mousedown(function () {
		//фокус на указатель, получаем данные об указателе
		var $el = $(this),
			$column = $el.parents('.drag'),
			max_value = $column.outerWidth(true) - $el.outerWidth(true),
			mmove = true,
			min_input_value = parseInt($column.attr('min'), 10),
			range_max = parseInt($column.attr('max'), 10) - min_input_value;
		
		if ($el.parents('.column').index() === 2) {
			return false;
		}
		$doc_ob.mousemove(function (event) {
			//двигаем указатель
			var value = move_value($el, event.pageX),
				input_value = 0,
				input_value2 = 0;
			$el.css('left', value);
			drag_line($el);
			if (max_value === 0) {
				return false;
			}
			input_value = parseInt((range_max * value) / max_value, 10) + min_input_value;
			if ($el.parents('.column').index() === 0) {
				input_value = Math.round(input_value / 1000) * 1000;
			}
			//подставляем новые значения в поля формы
			$el.parents('.column').find('input').val(input_value);
			formula_set();
		});
	});
	
	$doc_ob.mouseup(function () {
		//сброс событий с указателя
        $(this).off('mousemove');
	});
	
	$('.column').eq(0).find('input').change();
	
	function creat_lb(data) {
		var tpl,
			fin_tpl,
			height_content,
			speed;
		if ($('#lb_box').length  > 0) {
			speed = 0;
			$('#lb_box').remove();
		} else {
			speed = 400;
		}
		tpl = document.getElementById('lb').innerHTML;
		fin_tpl = tpl.split('{*content*}').join(data);
		$('body').append(fin_tpl).addClass('block_scrool');

		height_content = $('#lb_wrap_scroll').outerHeight(true);
		$('#lb_wrap').height(height_content);
		if (!(height_content < ($(window).height() * 0.8))) {
			$('#lb_wrap_scroll').addClass('scroll');
		}
		$('#lb_box').animate({'opacity': 1}, speed);
	}

	$doc_ob.on('click', '#lb_close', function () {
		$('#lb_box').animate({'opacity': 0}, 400, function () {
			$(this).remove();
		});
		$('body').removeClass('block_scrool');
	});

	$doc_ob.on('click', '#lb_close_ico', function () {
		$('#lb_close').click();
	});
	
	$doc_ob.on('click', '#make_order', function () {
		var form_str = $('#calc_form').serialize();
		if ($('#chek_box').hasClass('active')) {
			$.ajax({
				type: 'post',
				url: '../ajax.php',
				data: form_str,
				success: function (msg) {
					creat_lb(msg);
					$('#lb_time').text(count_mounth(parseInt($('.column').eq(1).find('input').val(), 10)));
					$('#lb_money').text(count_rubl(parseInt($('.column').eq(0).find('input').val(), 10)));
				}
			});
		} else {
			$('#confirm_check').addClass('red');
			setTimeout(function () {
				$('#confirm_check').removeClass('red');
			}, 500);
			
		}
	});
	$('head').append('<script type="text/tpl" id="lb"><div id=lb_box><div id=lb_close></div><div id=lb_wrap><svg id=lb_close_ico viewBox="0 0 20 20"><path d="M 0,0 L 20,20 M 0,20 L 20,0" stroke-width=4></path></svg><div id=lb_wrap_scroll>{*content*}</div></div></div></script>');
	
	$doc_ob.on('click', '#confirm_check p span', function () {
		$.ajax({
			type: 'post',
			url: '../offerta.php',
			success: function (msg) {
				creat_lb(msg);
			}
		});
	});
	$doc_ob.on('click', '#chek_box', function () {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$(this).addClass('active');
		}
	});
});