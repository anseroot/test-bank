<!DOCTYPE html>
<html lang=ru>
<head>
	<meta charset=UTF-8>
	<title>Document</title>
	<link rel=stylesheet href=../css/all.css>
</head>
<body>
<div class=align>
	<h1 class=bg>Расчет параметров кредита по персональному предложению кредита "Для своих"</h1>
	<div id=calc_view class=bg>
		<p>Выберите параметры кредитования и нажмите кнопку "Оформить заявку"</p>
		<form action=/ method=post name=calc_form id=calc_form>
		<div class=fl>
			<div class=column>
				<div class=fl>
					<p>Размер кредита</p>
					<div class=fl>
						<input type=text name=money value=25000>
						<span class=rubl>a</span>
					</div>
				</div>
				<div class=drag min="25000" max="500000">
					<div class=drag_line></div>
					<div class=drag_pointer>
						<svg viewBox="0 0 10 10">
							<path d=" M 4,0 L 1,5 L 4,10 M 6,0 L 9,5, L 6,10" />
						</svg>
					</div>
				</div>
			</div>
			<div class=column>
				<div class=fl>
					<p>Срок кредита</p>
					<div class=fl>
						<input type=text name=time value=12>
						<span class=count_mount>Месяцев</span>
					</div>
				</div>
				<div class=drag min=12 max=48>
					<div class=drag_line></div>
					<div class=drag_pointer>
						<svg viewBox="0 0 10 10">
							<path d=" M 4,0 L 1,5 L 4,10 M 6,0 L 9,5, L 6,10" />
						</svg>
					</div>
				</div>
			</div>
			<div class=column>
				<div class=fl>
					<p>Ежемесячный платеж</p>
					<div class=fl>
						<input type=text name=month disabled>
						<span class=rubl>a</span>
					</div>
				</div>
				<div class=drag>
					<div class=drag_line></div>
					<div class=drag_pointer>
						<svg viewBox="0 0 10 10">
							<path d=" M 4,0 L 1,5 L 4,10 M 6,0 L 9,5, L 6,10" />
						</svg>
					</div>
				</div>
			</div>
		</div>
		</form>
	</div>
	
	<div class=fl id=info_box>
		<div id=result_view class=bg>
			<div class="result_line fl">
				<p>Ставка по кредиту</p>
				<p>18,00%</p>
			</div>
			<div class="result_line fl">
				<p>Ежемесячный платеж</p>
				<p id="mount_result" class=fl>
					<span></span>
					<span></span>
				</p>
			</div>
			<div class="result_line fl">
				<p>Срок кредита</p>
				<p id=mount_count class=fl>
					<span></span>
					<span></span>
				</p>
			</div>
			<div class="result_line fl">
				<p>Размер кредита</p>
				<p id=money_get class=fl>
					<span></span>
					<span></span>
				</p>
			</div>
		</div>
		<div id=confirm_view class=bg>
			<div id=make_order>Оформить заявку</div>
			<div id=confirm_check class=fl>
				<div id=chek_box class="fl active">
					<svg viewBox="0 0 10 10">
						<path d="M 1,5 L 4,9 L 9,1" />
					</svg>
				</div>
				<p>Я согласен с <span>условиями кредита</span></p>
			</div>
		</div>
	</div>
</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src=../js/all.js></script>
</body>
</html>